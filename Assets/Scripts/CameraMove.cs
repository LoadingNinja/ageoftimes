using System.Collections;
using System.Collections.Generic;
using Extensions;
using UnityEngine;
using UnityEngine.Serialization;

public class CameraMove : MonoBehaviour
{
    public Transform cameraTransform;
    public string leftRightAxis = "Horizontal";
    public string forwardBackwardAxis = "Vertical";
    public string zoomAxis = "";
    public float moveSpeed = 0.3f;
    public float zoomSpeed = 1;
    
    private float zoomLevel = 1;
    private float manRoll = 0;
    private float manPitch = 1;
    private float manYaw = 0;

    // Update is called once per frame
    void Update()
    {
        // Init fast access vars
        
        var position = cameraTransform.position;
        
        // TODO: Update Manual Camera Rotation

        // Zoom position and following camera roll
        
        var zoomHeight = zoomLevel*zoomLevel;
        var dZoomHeight = zoomHeight - position.y;
        var zoomPitch = 25;
        
        // Camera Movement
        
        var forward = cameraTransform.forward;
        Vector2 direction = new Vector2(forward.x, forward.z).normalized;
        Vector2 vecMove = Vector2.zero;
        vecMove.x = Input.GetAxis(leftRightAxis) * moveSpeed;
        vecMove.y = Input.GetAxis(forwardBackwardAxis) * moveSpeed;
        vecMove.Align(direction);
        
        // Update camera data
        
        position += new Vector3(vecMove.x, dZoomHeight, vecMove.y);
        cameraTransform.position = position;
        Quaternion quatRotate = Quaternion.Euler(manPitch + zoomPitch, manYaw, manRoll);
        cameraTransform.rotation = quatRotate;
    }
    
}
