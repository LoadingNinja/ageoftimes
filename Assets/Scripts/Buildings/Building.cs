using System;
using UnityEngine;

namespace Buildings
{
    public abstract class Building : MonoBehaviour
    {
        public Transform objectTransform; 
        
        private Vector2 _gridPosition;
        private int _rotation;
        private Vector2 _scale;

        protected Building(Transform objectTransform, Vector2 gridPosition, int rotation = 0, Vector2 scale = default)
        {
            this.objectTransform = objectTransform;
            _gridPosition = gridPosition;
            _rotation = rotation;
            if (scale == default)
            {
                scale = Vector2.one;
            }
            _scale = scale;
        }

        public void Start()
        {
            
        }

        public void MoveTo(Vector2 gridPosition, int rotation)
        {
            _gridPosition = gridPosition;
            _rotation = rotation;
        }

        private void UpdateGameObject()
        {
            
        }
        
    }
}