using UnityEngine;

namespace Extensions
{
    public static class Vector2Extension
    {
        public static Vector2 Rotate(this Vector2 v, float degrees)
        {
            float radians = degrees * Mathf.Deg2Rad;
            float sin = Mathf.Sin(radians);
            float cos = Mathf.Cos(radians);
         
            float tx = v.x;
            float ty = v.y;
 
            return new Vector2(cos * tx - sin * ty, sin * tx + cos * ty);
        }

        public static float Angle(this Vector2 v)
        {
            var magnitude = Mathf.Sqrt(v.x * v.x + v.y * v.y);
            return Mathf.Asin(v.y / magnitude);
        }

        public static Vector2 Align(this Vector2 v, Vector2 other)
        {
            var rotateAmount = other.Angle() - v.Angle();
            rotateAmount *= Mathf.Rad2Deg;
            return v.Rotate(rotateAmount);
        }
        
    }
}